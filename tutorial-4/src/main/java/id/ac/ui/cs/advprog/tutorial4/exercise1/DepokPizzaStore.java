package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.ClamPizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.VeggiePizza;

public class DepokPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;
        PizzaIngredientFactory ingredientFactory = new DepokPizzaIngredientFactory();

        if (type.equals("Cheese")) {
            pizza = new CheesePizza(ingredientFactory);
            pizza.setName("Depok Cheese Pizza");
        } else if (type.equals("Clam")) {
            pizza = new ClamPizza(ingredientFactory);
            pizza.setName("Depok Clam Pizza");
        } else if (type.equals("Veggie")) {
            pizza = new VeggiePizza(ingredientFactory);
            pizza.setName("Depok Veggie Pizza");
        }

        return pizza;
    }
}
