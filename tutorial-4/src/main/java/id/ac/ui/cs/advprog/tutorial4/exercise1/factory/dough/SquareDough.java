package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SquareDough implements Dough {
    public String toString() {
        return "Square Dough";
    }
}
