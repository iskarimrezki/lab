package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class DecoratorMain {
    public static void main(String[] args){
        Food food = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(food.getDescription() + " $" + food.cost());

        Food food2 = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        food2 = FillingDecorator.TOMATO.addFillingToBread(food2);
        food2 = FillingDecorator.LETTUCE.addFillingToBread(food2);
        food2 = FillingDecorator.CUCUMBER.addFillingToBread(food2);
        System.out.println(food2.getDescription() + " $" + food2.cost());

        Food food3 = new ThinBunBurger();
        food3 = new BeefMeat(food3);
        food3 = new Cheese(food3);
        food3 = new TomatoSauce(food3);
        System.out.println(food3.getDescription() + " $" + food3.cost());
    }
}
