package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;

public class CompanyMain {
    public static void main(String[] args){
        Company tutupLapak = new Company();

        Employees employee = new Ceo("1.", 520000.00);
        tutupLapak.addEmployee(employee);

        employee = new Cto("2.", 340000.00);
        tutupLapak.addEmployee(employee);

        employee = new BackendProgrammer("3.", 95000.00);
        tutupLapak.addEmployee(employee);

        employee = new FrontendProgrammer("4.",69000.00);
        tutupLapak.addEmployee(employee);

        employee = new UiUxDesigner("5.", 190000.00);
        tutupLapak.addEmployee(employee);

        employee = new NetworkExpert("F", 87000.00);
        tutupLapak.addEmployee(employee);

        employee = new SecurityExpert("G", 72000.00);
        tutupLapak.addEmployee(employee);





        List<Employees> employeesList = tutupLapak.getAllEmployees();
        for (Employees employees: employeesList) {
            System.out.println(employees.getName() + " " + employees.getRole() +
                    " Salary = $" + employees.getSalary());
        }
        System.out.println("Net Salaries = $" + tutupLapak.getNetSalaries());

    }
}
